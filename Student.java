package com.company;

import java.util.Comparator;

public class Student {

    private String id;
    private String name;
    private int grade;

    public Student(){

    }

    public Student(String id, String name, int grade){
        this.id = id;
        this.name = name;
        this.grade = grade;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }


    public static Comparator<Student> StudentGradeComparator
            = new Comparator<Student>() {

        public int compare(Student student1, Student student2) {


            return student1.StudentCompareTo(student2);
        }

    };


    public int StudentCompareTo(Student s1)
    {
        Integer s1Grade = s1.getGrade();
        Integer s2Grade = this.getGrade();

        if(s2Grade.compareTo(s1Grade) == 0){

            if(s2Grade < s1Grade) return -1;
            else if(s2Grade > s1Grade) return 1;
            else return 0;
        }

        return s2Grade.compareTo(s1Grade);

    }
}
