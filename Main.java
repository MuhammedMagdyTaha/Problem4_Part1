package com.company;

import java.util.*;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.ArrayList;

public class Main  {

   static ArrayList<Student> students = new ArrayList<>();
   static HashMap<String, Student> studentsHashMap = new HashMap<>();

   public static  void getHighestGradeArrayList(ArrayList<Student> students){

       if (students.size() == 0 ){
           System.out.println("Empty List");
       }else {
           Collections.sort(students, new Comparator<Student>() {
               @Override
               public int compare(Student s1, Student s2) {
                   return Integer.compare(s2.getGrade(), s1.getGrade());
               }
           });

           System.out.println("The Highest Grade Student is: " + students.get(0).getName() + ", and His Grade is: " + students.get(0).getGrade());
       }
   }

    public static void getHighestGradeHashMap(HashMap<String, Student> studentsHashMap) {

        if (studentsHashMap.size() == 0) {
            System.out.println("Empty HashMap");
        } else {
            ArrayList<Entry<String, Student>> arrayList = new ArrayList<>(studentsHashMap.entrySet());
            int maxGradeEntryIndex = 0 ;
            for(int i = 0 ; i < arrayList.size() ; i++){
                if (arrayList.get(i).getValue().getGrade() > arrayList.get(maxGradeEntryIndex).getValue().getGrade()){
                    maxGradeEntryIndex = i;
                }
            }
            Student maxStudent = arrayList.get(maxGradeEntryIndex).getValue();
            System.out.println("The Highest Grade Student is: " + maxStudent.getName()
                    + ", and His Grade is: " + maxStudent.getGrade()
                    + ", and His ID is: " + maxStudent.getId());
        }
    }



    public static void main(String[] args) {

        students.add(new Student("1001","100 student", 100));
        students.add(new Student("1002","50 student", 50));
        students.add(new Student("1003","300 student", 300));
        students.add(new Student("1004","225 student", 225));
        students.add(new Student("1005","400 student", 400));
        students.add(new Student("1006","210 Student", 210));
        students.add(new Student("1007","180 student", 180));


        getHighestGradeArrayList(students);

        studentsHashMap.put("Ahmed", new Student("1001","100 student", 100));
        studentsHashMap.put("Tarek", new Student("1002","50 student", 50));
        studentsHashMap.put("saied", new Student("1003","300 student", 300));
        studentsHashMap.put("Mohammed", new Student("1004","225 student", 225));
        studentsHashMap.put("Magdi", new Student("1005","500 student", 500));
        studentsHashMap.put("Essam", new Student("1006","210 student", 210));

        getHighestGradeHashMap(studentsHashMap);
    }
}



